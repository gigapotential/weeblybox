FROM python:3.6-stretch
COPY filesharing /opt/weeblybox/filesharing
COPY Pipfile /opt/weeblybox/Pipfile
COPY Pipfile.lock /opt/weeblybox/Pipfile.lock
WORKDIR /opt/weeblybox
RUN pip install pipenv
RUN pipenv install
EXPOSE 8000
CMD cd filesharing && \
    . `pipenv --venv`/bin/activate && \
    gunicorn --bind 0.0.0.0:8000 filesharing.wsgi
