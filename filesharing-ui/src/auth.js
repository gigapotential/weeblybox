import axios from 'axios'
import Client from './client'

class Auth {

  login(username, password) {
      var url = Client.BASE_URL + "/apis/user/login"
      console.log("URL:", url)
      return axios.post(url, {
          username: username,
          password: password
      }).then((response) => {
          this.setToken(response.data)
          return response
      })
  }

  logout() {
      var url = Client.BASE_URL + "/apis/user/logout"
      console.log("URL:", url)
      return axios.post(url, {}, {
          headers: this.getAuthHeaders()
      }).then((response) => {
          this.removeToken()
          return response
      })
  }

  isLoggedIn() {
      return this.getToken() !== null
  }

  getAuthHeaders() {
      return {
        "X-JWT": this.getToken()
      }
  }

  removeToken() {
      localStorage.removeItem("token")
  }

  setToken(token) {
      localStorage.setItem("token", token)
  }

  getToken() {
      return localStorage.getItem("token")
  }

}

export default Auth;
