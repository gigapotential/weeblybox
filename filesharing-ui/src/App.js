import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Switch, Route } from 'react-router-dom'
import Login from './components/Login'
import Logout from './components/Logout'
import AppContainer from './components/AppContainer'


class App extends Component {
  render() {
    return (
      <BrowserRouter >
        <div className="center-block" style={{maxWidth:"70%", marginTop:"1%"}} >
          <Switch>
            <Route exact path='/' component={Login}/>
            <Route path='/login' component={Login}/>
            <Route path='/logout' component={Logout}/>
            <Route path='/app' component={AppContainer}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
