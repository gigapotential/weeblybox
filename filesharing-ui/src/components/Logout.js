import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {Alert} from 'react-bootstrap'
import Auth from '../auth'
const auth = new Auth()

class Logout extends Component {

    constructor(props) {
        super(props)
        this.state = {
            error: false,
            message: ""
        }
        auth.logout()
          .then((r) => {
              console.log("Logged out successfully!")
              //this.props.history.push("/login")
          })
          .catch((e) => {
              this.setState({error: true, message: "Failed to logout"})
              console.error('Failed to logout')
          })
    }

    render() {
        if(this.state.error) {
            return (
              <Alert>
                   {this.state.message} <Link to="/logout"> Logout Again </Link>
              </Alert>
            )
        } else {
        return (
            <Alert>
                I get Logged out but I get up again and <Link to="/login"> Login </Link>
            </Alert>
        )
      }
    }
}

export default Logout
