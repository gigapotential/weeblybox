import React, { Component } from 'react';
import {FormControl, FormGroup, ControlLabel, Button, Alert} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Auth from '../auth'

class Login extends Component {

  constructor(props) {
      super(props)
      this.state = {
        username: null,
        password: null,
        error: false,
        message: ""
      }
      this.auth =  new Auth()
  }

  handleUsernameChange = (e) => {
      this.setState({username: e.target.value})
  }

  handlePasswordChange = (e) => {
      this.setState({password: e.target.value})
  }

  handleSubmit = (e) => {
      e.preventDefault();
      this.setState({error: false, message: ""})
      this.auth.login(this.state.username, this.state.password)
        .then((response) => {
            console.log("Success:", response)
            this.forceUpdate()
        })
        .catch((error) => {
            console.log("Error:", error.response)
            this.setState({error: true, message: "Login failed"})
        })
  }

  render() {
      var ShowAlert = this.state.error ? <Alert bsStyle="danger">{this.state.message} </Alert> : <div />;
      var LoginForm = (
          <div style={{maxWidth: "300px"}} className="center-block">
              <form onSubmit={this.handleSubmit}>
                  <FormGroup
                    controlId="username">
                      <ControlLabel>Username</ControlLabel>
                      <FormControl
                        type="text"
                        value={this.state.username || ""}
                        placeholder="Username"
                        onChange={this.handleUsernameChange}
                      />
                  </FormGroup>
                  <FormGroup
                    controlId="password">
                      <ControlLabel>Password</ControlLabel>
                      <FormControl
                        type="password"
                        value={this.state.password || ""}
                        placeholder="Password"
                        onChange={this.handlePasswordChange}
                      />
                  </FormGroup>

                  <Button type="submit">Login</Button>
              </form>
              {ShowAlert}
          </div>
      );

      if(this.auth.isLoggedIn()) {
          return <Redirect to="/app" />
      }
      return LoginForm;
  }
}

export default Login;
