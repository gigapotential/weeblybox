import React, { Component } from 'react';
import {Image} from 'react-bootstrap'
import Client from '../client'

class FilePreview extends Component {

  constructor(props) {
      super(props)
      this.client = new Client()
  }

  render() {
    var imageTypes = ["image/jpeg", "image/jpg", "image/png",
                      "image/gif", "image/svg"]

    var preview = (<div> {this.props.file.name} </div>)
    if(imageTypes.includes(this.props.file.content_type)) {
        var url = this.client.getDownloadUrl(this.props.file)
        preview = <Image alt={this.props.file.name}
                         height="200"
                         src={url} rounded />
    }

    return preview;
  }
}

export default FilePreview;
