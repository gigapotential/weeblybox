import React, { Component } from 'react';
import {Navbar, Nav, NavItem, FormControl,
        InputGroup, Glyphicon, Media} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Auth from '../auth'
import Client from '../client'

import FileContainer from './FileContainer'

class AppContainer extends Component {

  constructor(props) {
      super(props)
      this.auth = new Auth()
      this.client = new Client()
      this.state = {
          files: [],
          query: ""
      }
      this.getFiles(this.state.query)
  }

  getFiles = (query) => {
      return this.client.getFiles(query)
        .then((files) => {
            console.log("FILES:", files)
            this.setState({files: files})
        })
        .catch((error) => {
            console.log("ERROR Fetching files:", error.response)
        })
  }

  handleDelete = (f) => {
      console.log("DELETE:", f)
      this.client.deleteFile(f)
        .then((response) => {
            console.log("Successfully deleted file:", f)
            this.getFiles(this.state.query)
        }).catch((error) => {
            console.log("Failed to delete file:", f)
        })
  }

  handleDownload = (f) => {
      console.log("DOWNLOAD:", f)
      window.open(this.client.getDownloadUrl(f), "_blank")
  }

  handleSearch = (e) => {
      var query = e.target.value
      console.log("SEARCH:", query)
      this.setState({query: query})
      this.getFiles(query)
  }

  handleRefresh = (e) => {
      this.getFiles(this.state.query)
  }

  handleLogout = (e) => {
      this.props.history.push("/logout")
  }

  render() {

    if(!this.auth.isLoggedIn()) {
        return <Redirect to='/login' />
    }

    return (
      <div >
        <Navbar>
          <Navbar.Header>
             <Navbar.Brand>
                <Media>
                  <Media.Left>
                    <img width="60px" src="/static/images/Small-Blue-Logotype.png" alt="logo" />
                    <b>box</b>
                  </Media.Left>
                </Media>
             </Navbar.Brand>
          </Navbar.Header>

          <Navbar.Form  pullLeft>
              <InputGroup >
                  <FormControl type="text" onChange={this.handleSearch}/>
                  <InputGroup.Addon>
                      <Glyphicon glyph="search" />
                  </InputGroup.Addon>
              </InputGroup>

          </Navbar.Form>

          <Nav>
              <NavItem onClick={this.handleRefresh}>
                  <Glyphicon glyph="refresh" />
              </NavItem>
          </Nav>
          <Nav>
              <NavItem onClick={this.handleLogout} > Logout </NavItem>
          </Nav>

        </Navbar>

        <FileContainer
            onDelete={this.handleDelete}
            onDownload={this.handleDownload}
            files={this.state.files} />
      </div>
    );
  }
}

export default AppContainer;
