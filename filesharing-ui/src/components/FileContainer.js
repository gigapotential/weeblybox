import React, { Component } from 'react';
import {Table, Button, Glyphicon} from 'react-bootstrap'
import FilePreview from './FilePreview'

class FileContainer extends Component {
  render() {
    var items = this.props.files.map((f, idx) => {
        return (
            <tr key={idx}>
                <td>{idx+1}</td>
                <td><FilePreview file={f} /></td>
                <td>{f.size}</td>
                <td>{f.content_type}</td>
                <td style={{wordWrap: "break-word"}}>{f.hash}</td>
                <td>
                    <Button onClick={() => this.props.onDownload(f)}
                            style={{margin: '2%'}}>
                        <Glyphicon glyph="cloud-download" />
                    </Button>
                    <Button onClick={() => this.props.onDelete(f)}
                            style={{margin: '2%'}}>
                        <Glyphicon glyph="trash" />
                    </Button>
                </td>
            </tr>
        )
     })
    return (
      <Table responsive {...this.props}>
          <thead>
              <tr>
                  <th>#</th>
                  <th>Preview</th>
                  <th>Size</th>
                  <th>Type</th>
                  <th>Hash</th>
                  <th>Actions</th>
              </tr>
          </thead>
          <tbody>
              {items}
          </tbody>
      </Table>
    );
  }
}

export default FileContainer;
