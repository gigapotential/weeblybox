import axios from 'axios'
import Auth from "./auth"

class Client {

    constructor() {
        this.auth = new Auth()
    }

    getFiles(query) {
        return axios.get(Client.BASE_URL + "/apis/files?q=" + query, {
            headers: this.auth.getAuthHeaders()
        }).then(
          (response) => {
              return response.data
        })
    }

    deleteFile(f) {
        return axios.delete(Client.BASE_URL + "/apis/files/" + f.id, {
            headers: this.auth.getAuthHeaders()
        })
    }

    getDownloadUrl(f) {
        return Client.BASE_URL +
               "/apis/files/download/" + f.id +
               "?token=" + this.auth.getToken()
    }

}

Client.BASE_URL  = "http://weeblybox-service:8000"

export default Client;
