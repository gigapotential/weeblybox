from django.core.files.uploadhandler import FileUploadHandler
from django.core.files.uploadedfile import UploadedFile
from django.conf import settings
import os

class WeeblyUploadedFile(UploadedFile):
    def __init__(self, name, content_type, size, charset, content_type_extra=None):
        file = open(os.path.join(settings.FILE_UPLOAD_DIR, name), 'wb')
        super().__init__(file, name, content_type, size, charset, content_type_extra)

    def close(self):
        print("Closing file: ", self.name)
        self.file.close()


class WeeblyFileUploadHandler(FileUploadHandler):

    def __init__(self, request=None):
        FileUploadHandler.__init__(self, request)

    def new_file(self, *args, **kwargs):
        super().new_file(*args, **kwargs)
        self.file = WeeblyUploadedFile(self.file_name, self.content_type,
                                       0, self.charset,
                                       self.content_type_extra)

    def receive_data_chunk(self, raw_data, start):
        self.file.write(raw_data)

    def file_complete(self, file_size):
        print("File complete:", self.file_name, "size:", file_size)
        self.file.size = file_size
        self.file.seek(0)
        return self.file

    def upload_complete(self):
        print("Upload complete:", self.file_name)
