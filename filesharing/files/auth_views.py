from django.contrib.auth.models import User
from files.models import Token
from django.conf import settings
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.http import HttpResponseNotAllowed
import jwt
import uuid
import json

from .middleware import get_user_id_and_token_id

def login_required(function):
    def wrapper(request, *args, **kwargs):
        if request.user is None or not request.user.is_authenticated:
            return HttpResponse(status=401)
        return function(request, *args, **kwargs)
    return wrapper

def login(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])

    try:
        input = json.loads(request.body)
        username = input['username']
        password = input['password']
    except:
        return HttpResponse(status=401)

    user = authenticate(username=username, password=password)
    if user is not None:
        # Create token
        token_id = str(uuid.uuid4())
        token = Token(user=user, token_id=token_id)
        token.save()
        payload = {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "user_id": user.id,
            "token_id": token_id
        }
        encoded = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
        return HttpResponse(encoded, status=201)
    else:
        # Return an 'invalid login' error message.
        return HttpResponse(status=401)

def logout(request):
    user = request.user
    if user is None:
        return HttpResponse(status=204)
    if not user.is_authenticated:
        return HttpResponse(status=204)

    _, token_id = get_user_id_and_token_id(request)

    Token.objects.filter(token_id=token_id).delete()
    return HttpResponse(status=204)
