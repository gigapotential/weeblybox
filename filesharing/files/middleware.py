from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User
from files.models import Token
from django.conf import settings
import jwt

def get_user_id_and_token_id(request):
    jwt_token = request.META.get('HTTP_X_JWT') or request.GET.get('token')
    decoded = jwt.decode(jwt_token, settings.SECRET_KEY, algorithms=['HS256'])
    return decoded["user_id"], decoded["token_id"]

class AuthJWTMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def get_user(self, request):
        try:
            user_id, token_id = get_user_id_and_token_id(request)
            users = list(User.objects.filter(id=user_id).all())
            if len(users) == 0:
                return AnonymousUser()
            tokens = list(Token.objects.filter(user=users[0]).filter(token_id=token_id).all())
            if len(tokens) == 0:
                return AnonymousUser()
            return users[0]
        except Exception as e:
            print("EXCEPTION:", e)
            return AnonymousUser()

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        request.user = self.get_user(request)
        print("Current USER: ", request.user)
        response = self.get_response(request)
        # Code to be executed for each request/response after
        # the view is called.

        return response
