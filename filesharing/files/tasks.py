from celery import task
from celery.utils.log import get_task_logger
from django.conf import settings
import os
import subprocess

from .models import FileModel

logger = get_task_logger(__name__)

@task
def delete_file_task(id):
    logger.info("Deleting file: {0}".format(id))
    filemodels = list(FileModel.objects.filter(id=id))
    if len(filemodels) == 0:
        logger.info("File model not found {0}".format(id))
        return
    filemodel = filemodels[0]
    try:
        os.remove(filemodel.path)
        logger.info("File deleted successfully {0}".format(id))
    except:
        logger.error("File delete failed")

@task
def compute_file_sha1(id):
    logger.info("Computing SHA1 for file: {0}".format(id))
    filemodels = list(FileModel.objects.filter(id=id))
    if len(filemodels) == 0:
        logger.info("File model not found {0}".format(id))
        return
    filemodel = filemodels[0]
    if not os.path.isfile(filemodel.path):
        logger.error("File not found: {0} - {1}".format(filemodel.id, filemodel.path))
        return
    try:
        completed_process = subprocess.run(["sha1sum", filemodel.path],
                                            stdout=subprocess.PIPE,
                                            check=True)
        hash = completed_process.stdout.decode("utf-8").split(" ")[0]
        filemodel.hash = hash
        filemodel.save()
        logger.info("File {0} SHA1 hash: {1}".format(filemodel.path, hash))
    except Exception as e:
        logger.error("SHA1 computation failed for "\
                     "{0} - {1}: {2}".format(id, filemodel.path, e))
