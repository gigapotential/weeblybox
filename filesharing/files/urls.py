from django.urls import path
import files.views as views
import files.auth_views as auth_views

urlpatterns = [
    path('files', views.list_or_create_files),
    path('files/<uuid:id>', views.get_or_update_file),
    path('files/upload', views.upload),
    path('files/download/<uuid:id>', views.download),

    path('user/login', auth_views.login),
    path('user/logout', auth_views.logout)
]
