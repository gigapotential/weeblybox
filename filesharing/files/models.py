from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import uuid
import os

class FileModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    size = models.BigIntegerField()
    content_type = models.CharField(max_length=50)
    status = models.CharField(max_length=10)
    path = models.CharField(max_length=300)
    hash = models.CharField(max_length=300)
    public = models.BooleanField(default=False)
    deleted_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def create_filemodel(cls, uploaded_file):
        filemodel = FileModel(
            name=uploaded_file.name,
            size=uploaded_file.size,
            content_type=uploaded_file.content_type,
            path=os.path.join(settings.FILE_UPLOAD_DIR, uploaded_file.name),
            public=False
        )
        filemodel.save()
        return filemodel

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'size': self.size,
            'content_type': self.content_type,
            'public': self.public,
            'path': self.path,
            'hash': self.hash,
            'created_at': self.created_at,
            'deleted_at': self.deleted_at,
            'updated_at': self.updated_at
        }


class Token(models.Model):
    token_id = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
