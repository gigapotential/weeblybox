from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import StreamingHttpResponse
from django.http import HttpResponseNotAllowed
from django.conf import settings
from .auth_views import login_required

from datetime import datetime
import json

from .models import FileModel
from .weeblyfileuploadhandler import WeeblyFileUploadHandler
from .tasks import delete_file_task, compute_file_sha1

@login_required
def list_or_create_files(request):
    if request.method == "GET":
        return get_files(request)
    elif request.method == "POST":
        return post_file(request)
    else:
        # return 405
        return HttpResponseNotAllowed(['GET', 'POST'])

@login_required
def get_or_update_file(request, id):
    if request.method == "GET":
        return get_file(request, id)
    elif request.method == "PUT":
        return put_file(request, id)
    elif request.method == "DELETE":
        return delete_file(request, id)
    else:
        # return 405
        return HttpResponseNotAllowed(['GET', 'PUT', 'DELETE'])

def get_files(request):
    files = FileModel.objects.filter(deleted_at=None)
    query = request.GET.get('q', None)
    if query and len(query) > 0:
        files = files.filter(name__icontains=query).all()

    file_list = [ file.to_dict() for file in files ]
    return JsonResponse(file_list, safe=False)

def get_file(request, id):
    files = list(FileModel.objects.filter(id=id).filter(deleted_at=None))
    if len(files) == 0:
        return HttpResponse(status=404)
    return JsonResponse(files[0].to_dict())

def post_file(request):
    try:
        file_payload = json.loads(request.body)
        file = FileModel(**file_payload)
        file.save()
    except:
        return HttpResponse(status=400)
    return JsonResponse(file.to_dict(), status=201)

def put_file(request, id):
    print(request)
    return JsonResponse({request.method: 'weebly'})

def delete_file(request, id):
    files = list(FileModel.objects.filter(id=id))
    if len(files) == 0:
        return HttpResponse(status=404)
    file = files[0]
    file.deleted_at = datetime.now()
    file.save()
    delete_file_task.delay(file.id)
    return HttpResponse(status=204) # No content

@login_required
def upload(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(['POST'])
    request.upload_handlers = [ WeeblyFileUploadHandler(request) ]
    filemodels = [ FileModel.create_filemodel(uploaded_file)
                    for (key, uploaded_file) in request.FILES.items() ]
    # Create task to Compute Hash
    [ compute_file_sha1.delay(filemodel.id) for filemodel in filemodels ]
    # Respond with JSON
    filemodel_list = [ filemodel.to_dict() for filemodel in filemodels ]
    return JsonResponse(filemodel_list, safe=False, status=201)

@login_required
def download(request, id):
    files = list(FileModel.objects.filter(id=id))
    if len(files) == 0:
        return HttpResponse(status=404)
    filemodel = files[0]
    if filemodel.deleted_at is not None:
        return HttpResponse(status=404)

    def output_file():
        CHUNK_SIZE = 4096
        with open(filemodel.path, 'rb') as f:
            while True:
                chunk = f.read(CHUNK_SIZE)
                if len(chunk) == 0:
                    break
                yield chunk

    response = StreamingHttpResponse(output_file(),
                                     content_type=filemodel.content_type)
    response['Content-Length'] = filemodel.size
    response['Content-Disposition'] = 'attachment; filename="' + filemodel.name +'"'
    return response
