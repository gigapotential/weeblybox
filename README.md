**Deploy on Kubernetes:**

Deploy Postgres (& Rabbitmq) first so that when Weeblybox is deployed it runs the migration on database which exists.
Rabbitmq before Weeblybox so that any messages to queue are not missed.

1. [**postgres**] Create `postgres-secret` containing Postgres db, user, password:  

    `kubectl create -f kubernetes/postgres-secret-development.yaml`

2. [**postgres**] Create PVC for Postgres storage, StatefulSet for Postgres, and headless service for Postgres Pod so that DNS resolves `postgres-service`. Because of PVC definition a Persistent Volume will be created from storage class `standard` and will bind with PVC. Which is mounted in Postgres pod at `/var/lib/postgres/data/` :

    `kubectl create -f kubernetes/postgres.yaml`     

3. [**rabbitmq**] Create `rabbitmq-secret` contain Rabbitmq user & password:

    `kubectl create -f kubernetes/rabbitmq-secret-development.yaml`

4. [**rabbitmq**] Create PVC for Rabbitmq storage, Statefulset for Rabbitmq, and headless service for Rabbitmq Pod so that DNS resolves `rabbitmq-service`:

    `kubectl create -f kubernetes/rabbitmq.yaml`

5. [**weeblybox**] Create a Job which runs database migration using `postgres-secret`. Create a PVC `filesharing-pvc` which stores uploaded files. Create a Deployment which creates a Pod with two containers one serves API, other runs a celery worker.  Create `LoadBalancer` servcie to expose API.

    `kubectl create -f kubernetes/weeblybox.yaml`

6. [**weeblybox-ui**] Wait for `weeblybox-service` to assign External IP, which is needed by UI and is set in yaml. Edit `weeblybox-ui.yaml`: set `WEEBLYBOX_HOST` env value to external IP for `weeblybox-service`.  Then create Deployment for `weeblybox-ui` and expose it as another `LoadBalancer` service:  

    `kubectl create -f kubernetes/weeblybox-ui.yaml`

** Deploy Locally **

0. Install **postgresql**: `brew install postgresql`
1. Start **postgres** : `brew services start postgresql`
2. Install **rabbitmq** : `brew install rabbitmq`
3. Start **rabbitmq**: `brew services start rabbitmq`
4. Create **postgres** user `filesharing` with password `filesharing123`
5. Initiate new pipenv : `cd weeblybox; pipenv install`
6. Start new enviornment: `pipenv shell`
7. Run migrations: `cd filesharing; python manage.py migrate`
8. Create a user himanshu with password himanshu: `python manage.py createsuperuser`
9. Edit `/etc/hosts` file so that rabbitmq & postgres & api server are reachable by their names:

    127.0.0.1 postgres-service

    127.0.0.1 rabbitmq-service

    127.0.0.1 weeblybox-service

10. Run API server: `python manage.py runserver`
11. Run UI server: `cd weeblybox/filesharing-ui && yarn install && yarn start`

** Build Containers **

*API & Celery worker container*
1. `cd weeblybox`
2. `docker build -t gcr.io/weebly-himanshu/weeblybox .`
3. `docker push gcr.io/weebly-himanshu/weeblybox`

*UI container*
1. `cd weeblybox/filesharing-ui`
2. `docker build -t gcr.io/weebly-himanshu/weeblybox-ui .`
3. `docker push gcr.io/weebly-himanshu/weeblybox-ui`
